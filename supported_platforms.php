<?php
/* $Id$ */

$debian_platforms = array(
    'alpha' => 'alpha-hp-linux-gnu',
    'amd64' => 'x86_64-pc-linux-gnu',
    'arm' => 'arm-unknown-linux-gnu',
    'armel' => 'arm-unknown-linux-gnueabi',
    'hppa' => 'hppa-unknown-linux-gnu',
    'hurd-i368' => 'i486-pc-gnu',
    'i386' => 'i686-pc-linux-gnu',
    'ia64' => 'ia64-linux-gnu',
    'kfreebsd-amd64' => 'x86_64-pc-kfreebsd-gnu',
    'kfreebsd-i386' => 'i686-pc-kfreebsd-gnu',
    'm68k' => 'm68k-unknown-linux-gnu',
    'mips' => 'mips-unknown-linux-gnu',
    'mipsel' => 'mipsel-unknown-linux-gnu',
    'powerpc' => 'powerpc-linux-gnu',
    'ppc64' => 'ppc64-linux-gnu',
    's390' => 's390-ibm-linux-gnu',
    'sparc' => 'sparc-sun-linux-gnu'
);

$projects = array(
    'abc' => array('http://abcathome.com/', 'ABC@home'),
    'aps' => array('http://www.apsathome.org/', 'APS@Home'),
    'bbc_cpdn' => array('http://bbc.cpdn.org/', 'BBC Climate Change'),
    'boinc_alpha' => array('http://isaac.ssl.berkeley.edu/alpha/', 'BOINC alpha test'),
    'brats' => array('http://maxwell.dhcp.umsl.edu/brats/', 'BRaTS@Home'),
    'burp' => array('http://burp.boinc.dk/', 'BURP'),
    'chess960' => array('http://www.chess960athome.org/alpha/', 'Chess960@Home'),
    'cpdn' => array('http://climateprediction.net/', 'CPDN'),
    'cosmo' => array('http://cosmos.astro.uiuc.edu/cosmohome/', 'Cosmology@Home'),
    'depspid' => array('http://www.depspid.net/', 'DepSpid'),
    'docking' => array('http://docking.utep.edu/', 'Docking@Home'),
    'einstein' => array('http://einstein.phys.uwm.edu/', 'Einstein@Home'),
    'enigma' => array('http://enigma.no-ip.net:443/enigma/', 'Enigma@Home'),
    'eternity2' => array('http://eternity2.net/', 'Eternity2.net'),
    'gerasim' => array('http://www.gerasim.boinc.ru/Gerasim/', 'Gerasim@Home'),
    'hash' => array('http://boinc.banaan.org/hashclash/', 'HashClash'),
    'hydrogen' => array('http://hydrogenathome.org/', 'Hydrogen@Home'),
    'irealm' => array('http://www.intelligencerealm.com/aisystem/', 'Intelligence Realm'),
    'leiden' => array('http://boinc.gorlaeus.net/', 'Leiden Classical'),
    'lhc' => array('http://lhcathome.cern.ch/', 'LHC@home'),
    'malaria' => array('http://www.malariacontrol.net/', 'malaricontrol.net'),
    'milkyway' => array('http://milkyway.cs.rpi.edu/milkyway/', 'MilkyWay@home'),
    'nano' => array('http://www.nanohive-1.org/atHome/', 'NanoHive@Home'),
    'nqueens' => array('http://nqueens.ing.udec.cl/', 'NQueens Project'),
    'pirates' => array('http://pirates.spy-hill.net/', 'Pirates@Home'),
    'predictor' => array('http://predictor.scripps.edu/', 'Predictor@Home'),
    'prime' => array('http://www.primegrid.com/', 'PrimeGrid'),
    'ps3grid' => array('http://www.ps3grid.net/PS3GRID/', 'PS3GRID'),
    'neuron' => array('http://neuron.mine.nu/neuron/', 'Project Neuron'),
    'poem' => array('http://boinc.fzk.de/poem/', 'POEM@HOME'),
    'proteins' => array('http://biology.polytechnique.fr/proteinsathome/', 'proteins@home'),
    'qmc' => array('http://qah.uni-muenster.de/', 'QMC@HOME'),
    'ralph' => array('http://ralph.bakerlab.org/', 'RALPH@home'),
    'rcn' => array('http://dist.ist.tugraz.at/cape5/', 'Rectilinear Crossing No.'),
    'render' => array('http://server2.povaddict.com.ar/pov/', 'RenderFarm@Home'),
    'riesel' => array('http://boinc.rieselsieve.com/', 'RieselSieve'),
    'rnd' => array('http://arcoboinc.unex.es/rnd/', 'RND@home'),
    'rosetta' => array('http://boinc.bakerlab.org/rosetta/', 'Rosetta@home'),
    'scilink' => array('http://www.scilinc.org/SciLINC/', 'SciLINC'),
    'seasonal' => array('http://attribution.cpdn.org/', 'Seasonal Attribution'),
    'seti' => array('http://setiathome.berkeley.edu/', 'SETI@home'),
    'seti_beta' => array('http://setiweb.ssl.berkeley.edu/beta/', 'SETI@home Beta'),
    'sha1cs' => array('http://boinc.iaik.tugraz.at/sha1_coll_search/', 'SHA-1 Collision Search'),
    'simap' => array('http://boinc.bio.wzw.tum.de/boincsimap/', 'SIMAP'),
    'spinhenge' => array('http://spin.fh-bielefeld.de/', 'Spinhenge@home'),
    'sudoku' => array('http://dist2.ist.tugraz.at/sudoku/', 'Sudoku project'),
    'superlink' => array('http://cbl-link02.cs.technion.ac.il/superlinkattechnion/', 'Superlink@Technion'),
    'sztaki' => array('http://szdg.lpds.sztaki.hu/szdg/', 'SZTAKI Desktop Grid'),
    'tanpaku' => array('http://issofty17.is.noda.tus.ac.jp/', 'TANPAKU'),
    'lattice' => array('http://boinc.umiacs.umd.edu/', 'The Lattice Project'),
    'drtg' => array('http://hashbreaker.com:8700/tmrldrtg/', 'TMRL DRTG'),
    'tsp' => array('http://bob.myisland.as/tsp/', 'TSP'),
    'ufludis' => array('http://www.ufluids.net/', 'uFluids'),
    'vtu' => array('http://boinc.vtu.lt/vtuathome/', 'vtu@home'),
    'wanless2' => array('http://bearnol.is-a-geek.com/wanless2/', 'WEP-M+2 Project'),
    'wcg' => array('http://www.worldcommunitygrid.org/', 'World Community Grid'),
    'xtrem' => array('http://xw01.lri.fr:4320/', 'XtremLab'),
    'yoyo' => array('http://www.rechenkraft.net/yoyo/', 'yoyo@home')
);

$xml_dir = 'apps';
if (! is_dir($xml_dir))
    mkdir($xml_dir);

function apps_file($project) {
    global $xml_dir;
    return $xml_dir.'/'.$project.'_apps.xml';
}

function config_file($project) {
    global $xml_dir;
    return $xml_dir.'/'.$project.'_config.xml';
}

function get_project_apps($projects, $force_update = false) {
    global $xml_dir;

    // Only update apps files if $xml_dir was last accessed more than 12h ago.
    if ((time() - fileatime($xml_dir)) < 12*3600 and !$force_update)
        return;

    foreach ($projects as $key => $project)
        copy($project[0].'apps.php?xml=1', apps_file($key));
}

function get_project_config($projects, $force_update = false) {
    global $xml_dir;

    foreach ($projects as $key => $project)
        copy($project[0].'get_project_config.php', config_file($key));
}

function get_supported_platforms($projects) {
    $supported_platforms = array();

    foreach ($projects as $key => $project) {
        $apps_file = apps_file($key);
        if (! file_exists($apps_file) or filesize($apps_file) < 64)
            continue;

        $supported_platforms[$key] = array();

        $apps_xml = @simplexml_load_file($apps_file);

        // Continue if the apps file is not a valid XML file.
        if (! $apps_xml)
            continue;

        $platforms = array();
        foreach ($apps_xml->application as $application)
            foreach($application->version as $version)
                array_push($platforms, (string) $version->platform_short);
 
        $supported_platforms[$key] = $platforms;
    }
    return $supported_platforms;
}

function get_supported_debian_platforms($supported_platforms, $debian_platforms) {
    $supported_debian_platforms = array();
    foreach ($supported_platforms as $project => $platforms) {
        $supported_debian_platforms[$project] = array_intersect_key(
            array_flip($debian_platforms), array_flip($platforms));
    }
    return $supported_debian_platforms;
}

function print_debian_wiki_table($projects, $supported_debian_platforms) {
    global $debian_platforms;

    $count = array();

    // Create table headline with all Debian architectures.
    $out = '|| ||';
    foreach ($debian_platforms as $arch => $platform) {
        $out .= ' '.$arch.' ||';
        $count[$arch] = 0;
    }
    $out .= "\n";

    // Create a row for each project.
    foreach ($supported_debian_platforms as $project => $platforms) {
        $out .= sprintf('|| [%s %s] ||', $projects[$project][0],
            $projects[$project][1]);

        foreach ($debian_platforms as $arch => $platform) {
            if (in_array($arch, $platforms)) {
                $out .= '<style="background-color: #80FF80; '
                     .  'text-align: center;">x||';
                $count[$arch]++;
            }
            else
                $out .= ' ||';
        }
        $out .= "\n";
    }

    $last_line = '|| '.count($supported_debian_platforms) .' ||';
    foreach ($debian_platforms as $arch => $platform)
        $last_line .= '<style="text-align: center;">'.$count[$arch] .'||';

    date_default_timezone_set('UTC');
    $now = date('d.m.Y H:i:s e');
    print $out.$last_line."\n||<-18> last updated $now ||\n";
}

// main program logic

get_project_apps($projects, true);
//get_project_config($projects, true);

$supported_platforms = get_supported_platforms($projects);

$supported_debian_platforms = get_supported_debian_platforms(
    $supported_platforms, $debian_platforms);

print "<pre>\n";
print_debian_wiki_table($projects, $supported_debian_platforms);
print "</pre>\n";

?>
