#!/bin/bash
# Instructions documented at  wiki.debian.org/BOINC/ServerGuide
# in a single script form.
# Creates a BOINC project using the debian boinc-server-maker package.
# 
# Distributed as same licesence as BOINC
# Copyright: Debian BOINC Developers. 

# Set Variables.
# Password for write access to your prject database
pw=MYSQLPASSWORDFORBOINCUSER

# Name of the MySQL database
dbprojectname=projectname

#Address of host (via DNS or IP number) at which project server shall be reached
hosturl=http://1.2.3.4/

#Name of folder in which data shall be stored, also becomes part of project URL
fileprojectname=$dbprojectname

# More sensible nice project name
niceprojectname="Test@Home"

# Location at which sources shall be kept
installroot=/var/tmp/boinc


# Check variables
if [ -z $dbprojectname ] || [ -z $pw ]; then
echo "Variables for mysql database not set"
exit -1
fi

# mysql database creation.
echo "Starting creation of database, enter Mysql root password when prompted."
 
cat <<EOF | mysql -u root -p
DROP DATABASE IF EXISTS $dbprojectname;
CREATE DATABASE $dbprojectname;
CREATE USER 'boincadm'@'localhost' IDENTIFIED BY '$pw';
GRANT ALL PRIVILEGES ON $dbprojectname.* TO 'boincadm'@'localhost';
EOF


echo "Enter user password when prompted"
# create directory
[ -d $installroot ] || sudo mkdir $installroot

sudo PYTHONPATH="/usr/share/pyshared/Boinc/":$PYTHONPATH /usr/share/boinc-server/tools/make_project --url_base "$hosturl" --db_name "$dbprojectname" --db_user boincadm --delete_prev_inst --drop_db_first --db_passwd "$pw" --project_root "$installroot"/"$fileprojectname" --srcdir /usr/share/boinc-server/ "$fileprojectname" "$niceprojectname"


# Post creation steps

if [ -z "$installroot" -o -z "$fileprojectname" ]; then
  echo "Not all variables are set for the configuration"
  echo "Error, do not continue."
  exit

elif [ ! -d "$installroot"/"$fileprojectname" ]; then
  echo "The directory '$installroot/'$fileprojectname' is not existing" 
  echo "Error, do not continue."
  exit

else
  cd "$installroot"/"$fileprojectname"
  sudo chown root:boincadm  -R .
  sudo chmod g+w -R .
  sudo chmod 02770 -R upload html/cache html/inc html/languages html/languages/compiled html/user_profile
  sudo cp ${fileprojectname}.httpd.conf  /etc/apache2/sites-available/
  sudo a2ensite  ${fileprojectname}.httpd.conf 
  sudo /etc/init.d/apache2 reload
fi

if [ -d html/inc -a -d cgi-bin ]; then
  sudo chmod    o+x html/inc
  sudo chmod -R o+r html/inc
  sudo chmod    o+x html/languages/
  sudo chmod    o+x html/languages/compiled
else
  echo "You are not in your project directory"
  exit -1
fi

exit 0

