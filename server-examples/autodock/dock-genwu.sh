#!/bin/bash

# To be run on a client, actual docking.
# Each workunit contains following files
# 1. $RECEPTOR.*.map*
# 2. $RECEPTOR.pdbqt
# 3. $LIGAND.pdbqt
# 4. $LAGNAD_$RECEPTOR.dpf

set -e

origDir=$(pwd)

# Define names.

RECEPTOR=receptor
if [ -z "$LIGANDS" ]; then
    LIGANDS=ligands
fi

generated=Generated



# BOINC project details.
projectRoot=/tmp
app=wrapper
temp_wu=templates/wrapper_wu.xml
temp_res=template/wrapper_res.xml

downStage=$projectRoot/downloads

# USAGE

if [ -z $1 ]; then
cat <<EOF
Usage: dock-genwu.sh number-of-wu-needed
EOF
exit -1
fi

cd $LIGANDS
mkdir -p $generated
# Add receptor files to the staging area.
if [ ! -d $downStage ]; then
    mkdir $downStage
fi
cp $RECEPTOR.pdbqt $RECEPTOR.maps.* $RECEPTOR.*.map $downStage


# Copy files for each ligand and generate corresponfing workunit.
count=0

for pf in *.dpf; do
    if [[ $count -lt $1 ]]; then 

	base=${pf%%_*}
	
	cp $base.pdbqt $pf $downStage
	
	$projectRoot/bin/create_work --appname $app --wu_name $base --wu_template $temp_wu \
	    --result_template $temp_res $RECEPTOR.A.map $RECEPTOR.C.map $RECEPTOR.d.map $RECEPTOR.e.map $RECEPTOR.maps.fld \
	    $RECEPTOR.maps.xyz $RECEPTOR.NA.map $RECEPTOR.N.map $RECEPTOR.OA.map $RECEPTOR.pdbqt $RECEPTOR.SA.map \
	    $base.pdbqt ${base}_${RECEPTOR}.dpf 
    
	mv $base* $generated
	rm $downStage/$base*
	let count=$count+1
    
    else
	echo Specified number of workunits generated!!
    fi
done

echo "Creation of work units completed."
exit 0
	
                                                                          
