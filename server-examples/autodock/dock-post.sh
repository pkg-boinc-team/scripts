#!/bin/bash

RECEPTOR=receptor
pythonPath=/usr/share/pyshared

# Summarize results of the docking.

# The code to obtain all validated results should be here.
# ###########################################################
# cd $projectRoot
# echo Assimiliating all results.
# bin/sample_bitwise_validater --app $app --one_pass -d 4
# bin/sample_assimiliator --app $app --one_pass -d 4
############################################################

cd $resultDir

for f in *; do
    mv $f $f.dlg
done

echo Summarising...
python $pythonPath/AutoDockTools/Utilities24/summarize_results4.py \
	       -d . -b -a \
	       -o results.txt

echo Process completed, results.txt contains the summary of docking. The $resultDir contians dlgs.
exit 0
	
                                                                          
