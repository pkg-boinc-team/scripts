#!/bin/bash

# The code to run at server, prepares ligands for work
# generator.
# 
# Created by Dhananjay M Balan <dbalan at acm dot org>
# Based on Tomas Malinauskas' docker.sh 
# Website: http://users.ox.ac.uk/~jesu1458/docker/
#
# The woring directory should contain ligands.mol2, receptor.gpf, receptor.pdbqt files. 
# This script splits them into a file for each ligand which is stored in the 
# ligands/ subdirectory. The work-gen script should be run from here to genearate a workunit for each ligand.

set -e
origDir=$(pwd)
# Specify ligand set file name.

RECEPTOR=receptor

if [ ! -r $RECEPTOR.pdbqt -o ! -r $RECEPTOR.gpf ]; then
    echo Receptor files could not be found!!!
    exit -1
fi

echo Preparing the receptor...

echo autogrid4 -p $RECEPTOR.gpf -l $RECEPTOR.glg
autogrid4 -p $RECEPTOR.gpf -l $RECEPTOR.glg

echo "Done."


if [ -z "$LIGANDS" ]; then
    LIGANDS=ligands
fi

if [ ! -r "$LIGANDS.mol2" ]; then
	echo "Expected the file '$LIGANDS.mol2' to be located in current directory. This was not found and I don't know what to spilt."
	exit -1
fi

mkdir $LIGANDS

echo "Preparing atomic coordinates of the ligands..." 
cd $LIGANDS

csplit -ftmp -n4 -ks - '%^@.TRIPOS.MOLECULE%' '/^@.TRIPOS.MOLECULE/' '{*}' < $origDir/ligands.mol2

for f in *
do
	entryName=$(grep ZINC $f)
	if [ -z "$entryName" ]; then
		continue
	else
		mv -v $f $entryName.mol2
	fi
done

pythonPath=/usr/share/pyshared
# Prepare each ligand file for docking with AutoDock.
echo "Preparing ligands for docking..."
for f in *.mol2; do
	echo $f
	if [ -r $(echo $f|sed -e 's/.mol2$/.pdbqt/') ]; then continue; fi
	python $pythonPath/AutoDockTools/Utilities24/prepare_ligand4.py -l $f
done


if [ ! -r "$RECEPTOR.pdbqt" ]; then
	echo "Linking ../$RECEPTOR.pdbqt to local directory"
	ln -f ../$RECEPTOR.pdbqt .
	ln -f ../$RECEPTOR.maps.* .
	ln -f ../$RECEPTOR.*.map .
fi

echo Starting the preparation of docking parameter files.
for f in *.pdbqt; do
    base=$(basename $f .pdbqt)
    
    if [ "$base" = "$RECEPTOR" ]; then 
	echo Test
	continue
    fi
	
    echo "Preparing dpf for $base against $RECEPTOR"
    
    python $pythonPath/AutoDockTools/Utilities24/prepare_dpf4.py -l "$base.pdbqt" -r "$RECEPTOR.pdbqt" \
	    -p ga_num_evals=1000000 \
	    -p ga_pop_size=100 \
	    -p ga_run=30

done


echo "Done."
echo "Find the ligand files in the $origDir/ligands subdirectory."
echo 
echo "You can now create work units for BOINC."

exit 0

